#/bin/bash
set -e

proc=5
file="/Users/karollewandowski/Desktop/single_cable_cleat.cub"
# accelerogram="/Users/karollewandowski/mofem_install/users_modules_debug/basic_finite_elements/nonlinear_elasticity/examples/dam/accelerogram.in"
accelerogram1="/Users/karollewandowski/mofem_install/users_modules/basic_finite_elements/multimaterial_dynamics/accelerogram1.in"
accelerogram2="/Users/karollewandowski/mofem_install/users_modules/basic_finite_elements/multimaterial_dynamics/accelerogram2.in"
accelerogram3="/Users/karollewandowski/mofem_install/users_modules/basic_finite_elements/multimaterial_dynamics/accelerogram3.in"

/Users/karollewandowski/spack_view/bin/mofem_part -file_name $file -nparts $proc 

mpirun -np $proc /Users/karollewandowski/mofem_install/users_modules/basic_finite_elements/multimaterial_dynamics/multimaterial_dynamics -file_name out.h5m -ts_dt 0.005 -ts_max_time 4 -ts_type beuler -ts_max_snes_failures -1 -order 2 -is_quasi_static 0 -accelerogram_44 $accelerogram1 -accelerogram_33 $accelerogram2 -accelerogram_22 $accelerogram3 -default_material NEOHOOKEAN

