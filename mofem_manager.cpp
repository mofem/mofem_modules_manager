/** \file mofem_manager.cpp
 *
 */
/* MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <BasicFiniteElements.hpp>
using namespace MoFEM;

#include <ElasticMaterials.hpp>
#include <NonlinearElasticElementInterface.hpp>
#include <BasicBoundaryConditionsInterface.hpp>

#include <SurfacePressureComplexForLazy.hpp>

#ifdef WITH_MODULE_MORTAR_CONTACT
  #include <Mortar.hpp>
#endif

#ifdef WITH_MODULE_MFRONT_INTERFACE
  #include <MFrontGenericInterface.hpp>
#endif

using DomainEle = VolumeElementForcesAndSourcesCore;
using DomainEleOp = DomainEle::UserDataOperator;
using BoundaryEle = FaceElementForcesAndSourcesCore;
using BoundaryEleOp = BoundaryEle::UserDataOperator;
using PostProcEle = PostProcVolumeOnRefinedMesh;
using PostProcSkinEle = PostProcFaceOnRefinedMesh;

constexpr size_t SPACE_DIM = 3;

static char help[] = "...\n\n";


#include <MoFEMManager.hpp>
using namespace MoFEMManager;

int main(int argc, char *argv[]) {

  const string default_options = "-ksp_type fgmres \n"
                                 "-pc_type lu \n"
                                 "-pc_factor_mat_solver_type mumps \n"
                                 "-ksp_atol 1e-10 \n"
                                 "-ksp_rtol 1e-10 \n"
                                 "-snes_monitor \n"
                                 "-snes_max_it 100 \n"
                                 "-snes_linesearch_type bt \n"
                                 "-snes_linesearch_max_it 3 \n"
                                 "-snes_atol 1e-8 \n"
                                 "-snes_rtol 1e-8 \n"
                                 "-ts_monitor \n"
                                 "-ts_alpha_radius 1 \n"
                                 "-ts_monitor \n"
                                 "-mat_mumps_icntl_20 0 \n"
                                 "-mat_mumps_icntl_14 800 \n"
                                 "-mat_mumps_icntl_24 1 \n"
                                 "-mat_mumps_icntl_13 \n";

  string param_file = "param_file.petsc";
  if (!static_cast<bool>(ifstream(param_file))) {
    std::ofstream file(param_file.c_str(), std::ios::ate);
    if (file.is_open()) {
      file << default_options;
      file.close();
    }
  }

  MoFEM::Core::Initialize(&argc, &argv, param_file.c_str(), help);

  // Add logging channel for example
  auto core_log = logging::core::get();
  core_log->add_sink(
      LogManager::createSink(LogManager::getStrmWorld(), "MManager"));
  LogManager::setLog("MManager");
  MOFEM_LOG_TAG("MManager", "module_manager");

  try {

    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    MoFEM::Core core(moab);
    MoFEM::Interface &m_field = core;

    MFManager md(m_field);
    CHKERR md.getParams();

    Simple *simple = m_field.getInterface<Simple>();
    CHKERR simple->getOptions();

    if (md.isPartitioned)
      CHKERR simple->loadFile("");
    else
      CHKERR simple->loadFile("","");
    
    simple->getProblemName() = "MoFEM Manager module";
    simple->getDomainFEName() = "ELASTIC";

    //setup the modules
    boost::ptr_vector<GenericElementInterface> m_modules;

    // Basic Boundary Conditions module should always be first (dirichlet)
    m_modules.push_back(new BasicBoundaryConditionsInterface(
        m_field, "U", "MESH_NODE_POSITIONS", simple->getProblemName(),
        simple->getDomainFEName(), true, md.isQuasiStatic, nullptr,
        md.isPartitioned));

#ifdef WITH_MODULE_MORTAR_CONTACT
    m_modules.push_back(new MortarContactInterface(
        m_field, "U", "MESH_NODE_POSITIONS", true, md.isQuasiStatic));
    // simple->getBitRefLevel() = m_modules[1].getBitRefLevel();
#endif

    // // Nonlinear Elastic Element module
    m_modules.push_back(new NonlinearElasticElementInterface(
        m_field, "U", "MESH_NODE_POSITIONS", true, md.isQuasiStatic));

#ifdef WITH_MODULE_MFRONT_INTERFACE
    //TODO: 
    // m_modules.push_back(new MFrontGenericInterface(m_field));
#endif

    CHKERR md.setMainField();

    for (auto &&mod : m_modules) {
      mod.getCommandLineParameters();
      mod.addElementFields();
    }

    // build fields
    // simple->buildFields();
    CHKERR m_field.build_fields();
    for (auto &&mod : m_modules) 
      mod.createElements();

    Projection10NodeCoordsOnField ent_method(m_field, "MESH_NODE_POSITIONS");
    CHKERR m_field.loop_dofs("MESH_NODE_POSITIONS", ent_method);

    // CHKERR simple->defineFiniteElements();
    // CHKERR simple->defineProblem(md.isPartitioned);
    CHKERR m_field.build_finite_elements();
    CHKERR m_field.build_adjacencies(simple->getBitRefLevel());
    // CHKERR m_field.add_problem(simple->getProblemName());

    // auto *bcs = dynamic_cast<BasicBoundaryConditionsInterface *> (&
    //             *m_modules.begin());
    // auto dirichlet = bcs->dirichletBcPtr;
    // CHKERR dynamic_cast<DirichletDisplacementRemoveDofsBc &>(*dirichlet)
    //     .iNitialize();

    CHKERR md.buildDM();

    for (auto &&mod : m_modules) {
      mod.setOperators();
      mod.addElementsToDM(md.dM);
    }

    CHKERR DMSetUp(md.dM);

    md.monitorPtr->preProcessHook = []() { return 0; };
    md.monitorPtr->operatorHook = []() { return 0; };
    md.monitorPtr->postProcessHook = [&]() {
      MoFEMFunctionBeginHot;
      // auto ts_time = md.monitorPtr->ts_t;
      auto ts_step = md.monitorPtr->ts_step;
      if (ts_step % md.saveEveryNthStep == 0) {
        for (auto &&mod : m_modules) {
          mod.postProcessElement(ts_step);
          mod.updateElementVariables();
        }
      }

      MoFEMFunctionReturnHot(0);
    };

    //FIXME: needs more elegant solution
    auto t_type = md.getTSType();
    for (auto &&mod : m_modules) {
      mod.setupSolverFunctionTS(t_type);
      mod.setupSolverJacobianTS(t_type);
    }
#ifndef NDEBUG
    cout << "<<< checkMPIAIJWithArraysMatrixFillIn <<< " << endl;
    CHKERR m_field.getInterface<MatrixManager>()
        ->checkMPIAIJWithArraysMatrixFillIn<PetscGlobalIdx_mi_tag>(
            simple->getProblemName(), -2, -2, 0);
#endif // NDEBUG

    CHKERR md.tsSetup();
    CHKERR md.tsSolve();

  }
  CATCH_ERRORS;

  MoFEM::Core::Finalize();
  return 0;
}