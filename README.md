MoFEM Modules Manager module
========================

This user module provides a generic interface for combining multiple modules from [MoFEM](http://http://mofem.eng.gla.ac.uk/).


![gif](doc/mofem_gif.gif)

Install MoFEM Modules Manager
=========================
In order to use MoFEM Modules Manager in MoFEM, first the *mgis* library needs to be installed on the system. 
The most straightforward method is to use MoFEM's fork of the [Spack](https://github.com/likask/spack) package manager. 

```
spack install mofem-modules-manager
```

To install the Manager with additional modules try:

```
spack install mofem-modules-manager +mofem-mortar-contact +mofem-multifield-plasticity +mofem-mfront-interface
```

For the full list of currently supported modules try:
```
spack info mofem-modules-manager
```

For a more detailed instructions on installing MoFEM follow [this link](http://mofem.eng.gla.ac.uk/mofem/html/installation.html). 


Upon successfull installation one can trigger ctests by executing the following command in the build directory:
```
ctest -VV
```

Example dynamic analysis:

![gif](doc/dam_gif.gif)
