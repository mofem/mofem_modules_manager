/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

namespace MoFEMManager {

struct MFManager {

  int order;
  int saveEveryNthStep;
  PetscBool isQuasiStatic;
  PetscBool isExplicit;
  PetscBool isPartitioned;
 
  MoFEM::Interface &mField;
  SmartPetscObj<TS> tSolver;
  SmartPetscObj<DM> dM;
  
  boost::shared_ptr<std::vector<unsigned char>> boundaryMarker;

  // FIXME: should we have a global (static) post-processing on volume and skin,
  // probably yes
  //  boost::shared_ptr<PostProcEle> postProc;
  //  boost::shared_ptr<PostProcSkinEle> postProcSkin;

  boost::shared_ptr<FEMethod> monitorPtr;

  MFManager(MoFEM::Interface &m_field) : mField(m_field) {
    monitorPtr = boost::make_shared<FEMethod>();
    saveEveryNthStep = 1;
    order = 1;
    isPartitioned = PETSC_FALSE;
  }

  MoFEMErrorCode getParams();
  MoFEMErrorCode setMainField();
  MoFEMErrorCode buildDM();
  MoFEMErrorCode tsSetup();
  MoFEMErrorCode tsSolve();

  MoFEMErrorCode getEntsOnMeshSkin(Range &bc);
  auto getTSType();
};

auto MFManager::getTSType() {
  auto t_type = GenericElementInterface::IM2;
  if (isQuasiStatic)
    t_type = GenericElementInterface::IM;
  if (isExplicit)
    t_type = GenericElementInterface::EX;
  return t_type;
}

MoFEMErrorCode MFManager::getParams() {
  MoFEMFunctionBegin;
  isQuasiStatic = PETSC_FALSE;
  isExplicit = PETSC_FALSE;
  CHKERR PetscOptionsGetBool(PETSC_NULL, "-is_quasi_static", &isQuasiStatic,
                             PETSC_NULL);
  CHKERR PetscOptionsGetBool(PETSC_NULL, "-is_explicit", &isExplicit,
                             PETSC_NULL);
  CHKERR PetscOptionsGetInt(PETSC_NULL, "-order", &order, PETSC_NULL);
  CHKERR PetscOptionsGetInt(PETSC_NULL, "-output_every", &saveEveryNthStep,
                            PETSC_NULL);

  CHKERR PetscOptionsGetBool(PETSC_NULL, "-is_partitioned", &isPartitioned,
                             PETSC_NULL);
  MOFEM_LOG("WORLD", Sev::inform)
      << "Mesh Partition Flag Status: " << isPartitioned;

  MoFEMFunctionReturn(0);
}


MoFEMErrorCode MFManager::setMainField() {
  MoFEMFunctionBegin;
  // MoFEMFunctionReturnHot(0);

  // Select base
  enum bases { AINSWORTH, DEMKOWICZ, BERNSTEIN, LASBASETOPT };
  const char *list_bases[] = {"ainsworth", "demkowicz", "bernstein"};
  PetscInt choice_base_value = AINSWORTH;
  CHKERR PetscOptionsGetEList(PETSC_NULL, NULL, "-base", list_bases,
                              LASBASETOPT, &choice_base_value, PETSC_NULL);

  FieldApproximationBase base;
  switch (choice_base_value) {
  case AINSWORTH:
    base = AINSWORTH_LEGENDRE_BASE;
    MOFEM_LOG("WORLD", Sev::inform)
        << "Set AINSWORTH_LEGENDRE_BASE for displacements";
    break;
  case DEMKOWICZ:
    base = DEMKOWICZ_JACOBI_BASE;
    MOFEM_LOG("WORLD", Sev::inform)
        << "Set DEMKOWICZ_JACOBI_BASE for displacements";
    break;
  case BERNSTEIN:
    base = AINSWORTH_BERNSTEIN_BEZIER_BASE;
    MOFEM_LOG("WORLD", Sev::inform)
        << "Set AINSWORTH_BERNSTEIN_BEZIER_BASE for displacements";
    break;
  default:
    base = LASTBASE;
    break;
  }

  // Add displacement field
  CHKERR mField.add_field("U", H1, base, 3);

  // Add field representing ho-geometry
  CHKERR mField.add_field("MESH_NODE_POSITIONS", H1, AINSWORTH_LEGENDRE_BASE,
                          3);

  // Add entities to field
  CHKERR mField.add_ents_to_field_by_type(0, MBTET, "U");
  CHKERR mField.add_ents_to_field_by_type(0, MBTET, "MESH_NODE_POSITIONS");

  // Set approximation order to entities
  if (base == AINSWORTH_BERNSTEIN_BEZIER_BASE)
    CHKERR mField.set_field_order(0, MBVERTEX, "U", order);
  else
    CHKERR mField.set_field_order(0, MBVERTEX, "U", 1);
  CHKERR mField.set_field_order(0, MBEDGE, "U", order);
  CHKERR mField.set_field_order(0, MBTRI, "U", order);
  CHKERR mField.set_field_order(0, MBTET, "U", order);

  CHKERR mField.set_field_order(0, MBVERTEX, "MESH_NODE_POSITIONS", 1);
  // CHKERR mField.set_field_order(0, MBEDGE, "MESH_NODE_POSITIONS", 2);
  CHKERR mField.set_field_order(0, MBEDGE, "MESH_NODE_POSITIONS", 1);
  CHKERR mField.set_field_order(0, MBTRI, "MESH_NODE_POSITIONS", 1);
  CHKERR mField.set_field_order(0, MBTET, "MESH_NODE_POSITIONS", 1);

  // CHKERR simple->addDomainField("U", H1, base, SPACE_DIM);
  // CHKERR simple->addBoundaryField("U", H1, base, SPACE_DIM);
  // CHKERR simple->setFieldOrder("U", order);

  // CHKERR simple->addDataField("MESH_NODE_POSITIONS", H1, base, 3);
  // CHKERR simple->setFieldOrder("MESH_NODE_POSITIONS", 1);

  // auto *pipeline_mng = mField.getInterface<PipelineManager>();
  auto integration_rule = [](int, int, int approx_order) {
    return 2 * approx_order + 1;
  };

  // CHKERR pipeline_mng->setDomainRhsIntegrationRule(integration_rule);
  // CHKERR pipeline_mng->setDomainLhsIntegrationRule(integration_rule);
  // CHKERR pipeline_mng->setBoundaryRhsIntegrationRule(integration_rule);
  // CHKERR pipeline_mng->setBoundaryLhsIntegrationRule(integration_rule);

  // if (base != AINSWORTH_BERNSTEIN_BEZIER_BASE) {
  // }

  // Range skin_edges;
  // CHKERR getEntsOnMeshSkin(skin_edges);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MFManager::buildDM() {
  MoFEMFunctionBegin;

  auto simple = mField.getInterface<Simple>();
  DMType dm_name = "DMMOFEM";
  // Register DM problem
  CHKERR DMRegister_MoFEM(dm_name);
  dM = createSmartDM(mField.get_comm(), dm_name);
  CHKERR DMSetType(dM, dm_name);
  CHKERR DMMoFEMSetIsPartitioned(dM, isPartitioned);
  // Create DM instance
  CHKERR DMMoFEMCreateMoFEM(dM, &mField, simple->getProblemName().c_str(),
                            simple->getBitRefLevel());
  CHKERR DMSetFromOptions(dM);

  // mField.getInterface<ProblemsManager>()->buildProblemFromFields = PETSC_TRUE;

  MoFEMFunctionReturn(0);
}


MoFEMErrorCode MFManager::tsSetup() {
  MoFEMFunctionBegin;

  Simple *simple = mField.getInterface<Simple>();
  PipelineManager *pipeline_mng = mField.getInterface<PipelineManager>();
  ISManager *is_manager = mField.getInterface<ISManager>();

  auto set_time_monitor = [&](auto solver) {
    MoFEMFunctionBegin;
    boost::shared_ptr<ForcesAndSourcesCore> null;
    CHKERR DMMoFEMTSSetMonitor(dM, solver, simple->getDomainFEName(),
                               monitorPtr, null, null);
    MoFEMFunctionReturn(0);
  };

  auto set_dm_section = [&](auto dm) {
    MoFEMFunctionBeginHot;
    auto section = mField.getInterface<ISManager>()->sectionCreate(
        simple->getProblemName());
    CHKERR DMSetSection(dm, section);
    MoFEMFunctionReturnHot(0);
  };

  CHKERR set_dm_section(dM);

  auto D = smartCreateDMVector(dM);
  tSolver = MoFEM::createTS(mField.get_comm());
  
  //FIXME: for explicit you need to implement proper mass matrix for rhs operators
  if (isQuasiStatic || isExplicit) {
    CHKERR TSSetSolution(tSolver, D);
  } else {
    CHKERR TSSetType(tSolver, TSALPHA2);
    auto DD = smartVectorDuplicate(D);
    CHKERR TS2SetSolution(tSolver, D, DD);
  }

  CHKERR TSSetDM(tSolver, dM);

  //default max time is 1
  CHKERR TSSetMaxTime(tSolver, 1.0);
  CHKERR TSSetExactFinalTime(tSolver, TS_EXACTFINALTIME_MATCHSTEP);
  CHKERR TSSetFromOptions(tSolver);

  CHKERR set_time_monitor(tSolver);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MFManager::tsSolve() {
  MoFEMFunctionBegin;

  CHKERR TSSetUp(tSolver);
  CHKERR TSSolve(tSolver, NULL);

  // CHKERR VecGhostUpdateBegin(D, INSERT_VALUES, SCATTER_FORWARD);
  // CHKERR VecGhostUpdateEnd(D, INSERT_VALUES, SCATTER_FORWARD);
  // CHKERR DMoFEMMeshToLocalVector(dm, D, INSERT_VALUES, SCATTER_REVERSE);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MFManager::getEntsOnMeshSkin(Range &boundary_ents) {
  MoFEMFunctionBeginHot;

  Range body_ents;
  CHKERR mField.get_moab().get_entities_by_dimension(0, 3, body_ents);
  Skinner skin(&mField.get_moab());
  Range skin_ents;
  CHKERR skin.find_skin(0, body_ents, false, skin_ents);

  // filter not owned entities, those are not on boundary
  // Range boundary_ents;
  ParallelComm *pcomm =
      ParallelComm::get_pcomm(&mField.get_moab(), MYPCOMM_INDEX);
  if (pcomm == NULL) {
    SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
            "Communicator not created");
  }

  CHKERR pcomm->filter_pstatus(skin_ents, PSTATUS_SHARED | PSTATUS_MULTISHARED,
                               PSTATUS_NOT, -1, &boundary_ents);

  MoFEMFunctionReturnHot(0);
}

} // namespace MoFEMManager